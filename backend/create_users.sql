drop table if exists Users;
create table Users(
    username   varchar(255),
    password    varchar(255) not null,
    balance     decimal default 100 CHECK (balance >= 0),
    primary key(username)
);