drop table if exists ServiceProfiles;
create table ServiceProfiles(
    name    varchar(255),
    min_amount  decimal check (min_amount > 0),
    max_amount  decimal check (max_amount > 0),
    primary key (name)
);
COMMENT ON COLUMN ServiceProfiles.name IS '�������� ������';
COMMENT ON COLUMN ServiceProfiles.min_amount IS '����������� �����';
COMMENT ON COLUMN ServiceProfiles.max_amount IS '������������ �����';