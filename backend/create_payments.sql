drop table if exists Payments;
create sequence payments_seq start 1;
create table Payments(
    id  integer default nextval('payments_seq'),
    account varchar(40) not null,
    amount  decimal check (amount > 0),
    created timestamp default current_timestamp,
    created_by    varchar(255) not null,
    service_name    varchar(255) not null,
    primary key (id),
    foreign key (created_by) references users (username),
    foreign key (service_name) references serviceprofiles (name)
);
comment on column Payments.account is '����� �����';
comment on column Payments.amount is '�����';
comment on column Payments.created is '����� ��������';
comment on column Payments.created_by is '��� ������';
comment on column Payments.service_name is '�������� ������';