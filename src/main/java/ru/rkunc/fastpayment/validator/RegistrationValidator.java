/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.rkunc.fastpayment.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.rkunc.fastpayment.dao.UserDAO;
import ru.rkunc.fastpayment.domain.User;

/**
 *
 * @author rkunc
 */
@Component("registrationValidator")
public class RegistrationValidator implements Validator{

    @Autowired
    private UserDAO userDAO;
            
    private static final String EMAIL_PATTERN = 
		"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String PHONE_PATTERN = "^9\\d{9}";
    
    @Override
    public boolean supports(Class<?> type) {
        return User.class.equals(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", "label.validate.loginEmpty");
        
        User user = (User) o;
        if(user.getLogin().matches(EMAIL_PATTERN) && !user.getLogin().matches(PHONE_PATTERN)){
            errors.rejectValue("login", "label.validate.incorrectlogin");
        }
    }
}
