/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.rkunc.fastpayment.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.rkunc.fastpayment.domain.Payment;

/**
 *
 * @author rkunc
 */
@Component("paymentValidator")
public class PaymentValidator implements Validator{

    private static final String ACCOUNT_PATTERN = "^[0-9]{1,40}";
    private static final String AMOUNT_PATTERN = "^\\d+(\\.\\d{1,2})?";
    
    @Override
    public boolean supports(Class<?> type) {
        return Payment.class.equals(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "account", "label.validate.accountEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "amount", "label.validate.amountEmpty");
        
        Payment payment = (Payment)o;
        if(!payment.getAccount().isEmpty() && !payment.getAccount().matches(ACCOUNT_PATTERN)){
            errors.rejectValue("account", "label.validate.incorrectAccount");
        }
        if(payment.getAmount() != null && !payment.getAmount().toString().matches(AMOUNT_PATTERN)){
            errors.rejectValue("amount", "label.validate.incorrectAmount");
        }
        
    }

}
