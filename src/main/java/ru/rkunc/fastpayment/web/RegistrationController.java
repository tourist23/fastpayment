/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.rkunc.fastpayment.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.rkunc.fastpayment.domain.User;
import ru.rkunc.fastpayment.service.RegistrationService;

/**
 *
 * @author rkunc
 */
@Controller
@RequestMapping("registration")
public class RegistrationController {

    @Autowired
    @Qualifier("registrationValidator")
    private Validator userValidator;
    
    @Autowired
    private RegistrationService registrationService;
    
    @RequestMapping(method = RequestMethod.GET)
    @ModelAttribute("user")
    public User createRegistrationPage(ModelMap model){
        return new User();
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public String createUser(@ModelAttribute("user") User user, BindingResult result, ModelMap model)
            throws Exception
    {
        
        userValidator.validate(user, result);
        if(result.hasErrors()){
            return "registration";
        }
        if(registrationService.isExistsUser(user.getLogin())){
            result.rejectValue("login", "label.validate.existedlogin");
            return "registration";
        }
        
        User createdUser = registrationService.registerUser(user.getLogin());
        model.addAttribute("user", createdUser);
        model.addAttribute("successMsg", true);
       
        return "registration";
    }
}
