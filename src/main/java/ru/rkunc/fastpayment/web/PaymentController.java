/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.rkunc.fastpayment.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.Pack200;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import ru.rkunc.fastpayment.domain.Payment;
import ru.rkunc.fastpayment.domain.ServiceProfile;
import ru.rkunc.fastpayment.service.PaymentService;
import ru.rkunc.fastpayment.service.ServiceProfileService;

/**
 *
 * @author rkunc
 */
@Controller()
@SessionAttributes("services")
@RequestMapping("payment")
public class PaymentController {

    @Autowired
    private ServiceProfileService spService;
    
    @Autowired
    private PaymentService paymentService;
    
    @Autowired
    @Qualifier("paymentValidator")
    private Validator paymentValidator;
    
    @RequestMapping(value = "/getService", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ServiceProfile getService(@RequestParam(value = "serviceName") String serviceName, ModelMap model){
        List<ServiceProfile> services = (List<ServiceProfile>) model.get("services");
        for (ServiceProfile service : services) {
            if(service.getName().equals(serviceName)) {
                return service;
            }
        }
        return null;
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public String createPayment(@ModelAttribute("newPayment") Payment payment, BindingResult result, ModelMap model){
        paymentValidator.validate(payment, result);
        if(result.hasErrors()){
            return "payment";
        }
        
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        payment.setCreatedBy(auth.getName());
        
        if(!paymentService.compareAmountAndUserBalance(payment)){
            result.rejectValue("amount", "label.validate.amountAndBalance");
            return "payment";
        }
        if(!paymentService.compareAmountAndServiceLimits(payment)){
            result.rejectValue("amount", "label.validate.amountAndLimits");
            return "payment";
        }
        
        paymentService.addPayment(payment);
        
        model.addAttribute("successMsg", true);
        model.addAttribute("newPayment", new Payment());
        
        return "payment";
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public String createPaymentPage(ModelMap model) throws IOException{
        List<ServiceProfile> services = spService.getProfiles();
        model.addAttribute("services", services);
        model.addAttribute("newPayment", new Payment());
        return "payment";
    }
}
