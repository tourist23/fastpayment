/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.rkunc.fastpayment.web;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.rkunc.fastpayment.domain.Payment;
import ru.rkunc.fastpayment.domain.User;
import ru.rkunc.fastpayment.service.PaymentService;
import ru.rkunc.fastpayment.service.RegistrationService;

/**
 *
 * @author rkunc
 */
@Controller
public class CabinetController {

    @Autowired
    private RegistrationService userService;
    
    @Autowired
    private PaymentService paymentService;
    
    @RequestMapping("/")
    public String home() {
            return "redirect:/index";
    }
    
    @RequestMapping("/index")
    public String showCabinet(ModelMap model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.getUser(auth.getName());
        
        List<Payment> payments = paymentService.listPayments(user);
        
        model.addAttribute("user", user);
        model.addAttribute("payments", payments);
        return "cabinet";
    }
}
