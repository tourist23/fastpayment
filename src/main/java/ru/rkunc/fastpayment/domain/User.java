/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.rkunc.fastpayment.domain;

import java.math.BigDecimal;

/**
 *
 * @author rkunc
 */
public class User {
    
    private String login;
    private String password;
    private BigDecimal balance;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "User{" + "login=" + login + ", balance=" + balance + '}';
    }

    
}
