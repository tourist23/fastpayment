package ru.rkunc.fastpayment.domain;

import java.math.BigDecimal;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rkunc
 */
public class ServiceProfile{
    
    private String name;
    private BigDecimal minAmount;
    private BigDecimal maxAmount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(BigDecimal minAmount) {
        this.minAmount = minAmount;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    @Override
    public String toString() {
        return "ServiceProfile{" + "name=" + name + ", minAmount=" + minAmount + ", maxAmount=" + maxAmount + '}';
    }

}
