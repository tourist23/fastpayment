/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.rkunc.fastpayment.dao;

import ru.rkunc.fastpayment.domain.User;

/**
 *
 * @author rkunc
 */
public interface UserDAO {

    public void insert(User user);
    
    public User findByUsername(String username);
    
    public void update(User user);
}
