/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.rkunc.fastpayment.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.rkunc.fastpayment.domain.User;

/**
 *
 * @author rkunc
 */
@Repository
@Transactional(readOnly = true)
public class UserDAOImpl implements UserDAO{

    private JdbcTemplate jdbcTemplate;
    
    @Autowired
    public void setDataSource(DataSource dataSource){
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
    @Override
    @Transactional(readOnly = false)
    public void insert(User user) {
        if(user == null){
            throw new IllegalArgumentException("User is null");
        }
        jdbcTemplate.update(
                "insert into Users (username, password) values (?, ?)", 
                user.getLogin(), 
                user.getPassword());
    }

    @Override
    public User findByUsername(String username) {
        if(username == null){
            throw new IllegalArgumentException("Login is null");
        }
        List<User> users = jdbcTemplate.query(
                "select * from Users u where u.username = ?", 
                new UserMapper(),
                username
        );
        return !users.isEmpty() ? users.get(0) : null;
    
    }

    @Override
    @Transactional(readOnly = false)
    public void update(User user) {
        if(user == null){
            throw new IllegalArgumentException("User is null");
        }
        jdbcTemplate.update(
                "update Users u set password = ?, balance = ? where u.username = ?", 
                user.getPassword(),
                user.getBalance(),
                user.getLogin()
        );
    }
    
    private static final class UserMapper implements RowMapper<User>{

        @Override
        public User mapRow(ResultSet rs, int i) throws SQLException {
            User user = new User();
            user.setLogin(rs.getString("username"));
            user.setPassword(rs.getString("password"));
            user.setBalance(rs.getBigDecimal("balance"));
            return user;
        }
        
    }
}
