/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.rkunc.fastpayment.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.rkunc.fastpayment.domain.Role;

/**
 *
 * @author rkunc
 */
@Repository
@Transactional(readOnly = true)
public class RoleDAOImpl implements RoleDAO{

    private JdbcTemplate jdbcTemplate;
    
    @Autowired
    public void setDataSource(DataSource dataSource){
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
    @Override
    @Transactional(readOnly = false)
    public void insert(Role role) {
        String sql = "insert into Authorities (username, authority) values (?, ?)";
        jdbcTemplate.update(sql, role.getUsername(), role.getAuthority());
    }
    
    @Override
    public List<Role> findByUsername(String username) {
        List<Role> roles = jdbcTemplate.query(
                "select * from Authorities a where a.username = ?",
                new RoleMapper(),
                username
        );
        return roles;
    }

    private static class RoleMapper implements RowMapper<Role>{
        @Override
        public Role mapRow(ResultSet rs, int i) throws SQLException {
            Role role = new Role();
            role.setAuthority(rs.getString("authority"));
            role.setUsername(rs.getString("username"));
            return role;
        }
    }
}
