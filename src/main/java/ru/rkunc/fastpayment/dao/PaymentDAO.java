/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.rkunc.fastpayment.dao;

import java.util.List;
import ru.rkunc.fastpayment.domain.Payment;

/**
 *
 * @author rkunc
 */
public interface PaymentDAO {
    
    public void insert(Payment payment);
    
    public List<Payment> findByUsername(String username);
}
