/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.rkunc.fastpayment.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.rkunc.fastpayment.domain.Payment;

/**
 *
 * @author rkunc
 */
@Repository
@Transactional(readOnly = true)
public class PaymentDAOimpl implements PaymentDAO{

    private JdbcTemplate jdbcTemplate;
    
    @Autowired
    public void setDataSource(DataSource dataSource){
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
    @Override
    @Transactional(readOnly = false)
    public void insert(Payment payment) {
        jdbcTemplate.update(
                "insert into Payments (account, amount, created_by, service_name) values (?,?,?,?)", 
                payment.getAccount(), 
                payment.getAmount(),
                payment.getCreatedBy(),
                payment.getServiceName()
                );
    }

    @Override
    public List<Payment> findByUsername(String username) {
        return jdbcTemplate.query(
                "select * from Payments p where p.created_by = ?",
                new PaymentMapper(),
                username
        );
    }

    private static final class PaymentMapper implements RowMapper<Payment>{

        @Override
        public Payment mapRow(ResultSet rs, int i) throws SQLException {
            Payment payment = new Payment();
            payment.setId(rs.getLong("id"));
            payment.setAccount(rs.getString("account"));
            payment.setAmount(rs.getBigDecimal("amount"));
            payment.setCreated(rs.getTimestamp("created"));
            payment.setCreatedBy(rs.getString("created_by"));
            payment.setServiceName(rs.getString("service_name"));
            return payment;
        };
        
    }
}
