/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.rkunc.fastpayment.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.rkunc.fastpayment.domain.ServiceProfile;

/**
 *
 * @author rkunc
 */
@Repository
@Transactional(readOnly = true)
public class ServiceProfileDAOImpl implements ServiceProfileDAO{

    private JdbcTemplate jdbcTemplate;
    
    @Autowired
    public void setDataSource(DataSource dataSource){
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
    @Override
    public List<ServiceProfile> findAll() {
        return jdbcTemplate.query("select * from ServiceProfiles", new ServiceProfileMapper());
    }

    @Override
    public ServiceProfile findByName(String name) {
        List<ServiceProfile> profiles = jdbcTemplate.query(
                "select * from ServiceProfiles p where p.name = ?", 
                new ServiceProfileMapper(),
                name);
        return !profiles.isEmpty() ? profiles.get(0) : null;
    }
    
    private static final class ServiceProfileMapper implements RowMapper<ServiceProfile>{

        @Override
        public ServiceProfile mapRow(ResultSet rs, int i) throws SQLException {
            ServiceProfile sp = new ServiceProfile();
            sp.setName(rs.getString("name"));
            sp.setMinAmount(rs.getBigDecimal("min_amount"));
            sp.setMaxAmount(rs.getBigDecimal("max_amount"));
            return sp;
        }
        
    }
}
