/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.rkunc.fastpayment.service;

import ru.rkunc.fastpayment.domain.User;

/**
 *
 * @author rkunc
 */
public interface RegistrationService {
    
    public User registerUser(String login);
    
    public User getUser(String login);
    
    public boolean isExistsUser(String login);
}
