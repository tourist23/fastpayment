/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.rkunc.fastpayment.service;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.rkunc.fastpayment.dao.RoleDAO;
import ru.rkunc.fastpayment.dao.UserDAO;
import ru.rkunc.fastpayment.domain.Role;
import ru.rkunc.fastpayment.domain.User;

/**
 *
 * @author rkunc
 */
@Service
public class RegistrationServiceImpl implements RegistrationService{

    private static final Logger logger = LogManager.getLogger(RegistrationServiceImpl.class);
    
    @Autowired
    private UserDAO userDAO;
    
    @Autowired
    private RoleDAO roleDAO;
    
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Override
    @Transactional
    public User registerUser(String login) {
        logger.info("Регистрация пользователя с логином {}", login);
        if(login == null){
            throw new IllegalArgumentException("Login is null");
        }
        
        logger.info("Генерация пароля для пользователя {}", login);
        final String password = generatePassword();
        
        logger.info("Создание пользователя {} в базе", login);
        User user = createUser(login,password);
        userDAO.insert(user);
        
        Role role = createRole(login);
        roleDAO.insert(role);
        
        user.setPassword(password);
        
        logger.info("Пользватель {} успешно зарегестрирован", login);
        return user;
    }

    @Override
    public User getUser(String login) {
        return userDAO.findByUsername(login);
    }

    private Role createRole(String username){
        Role role = new Role();
        role.setUsername(username);
        role.setAuthority(CustomRole.USER.getName());
        return role;
    }
    
    private User createUser(String username, String password){
        User user = new User();
        user.setLogin(username);
        
        String encodedPassword = passwordEncoder.encode(password);
        user.setPassword(encodedPassword);
        
        return user;
    }
    
    private String generatePassword() {
        return RandomStringUtils.randomAlphanumeric(8);
    }

    @Override
    public boolean isExistsUser(String login) {
        logger.info("Проверка уникальности логина {}", login);
        User existedUser = userDAO.findByUsername(login);
        
        boolean result = existedUser != null;
        if(result){
            logger.info("Пользователь с логином {} уже существует", login);
        }else{
            logger.info("Логин {} уникален", login);
        }
        return result;
    }
    
    
}
