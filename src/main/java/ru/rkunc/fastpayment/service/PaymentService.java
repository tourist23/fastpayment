/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.rkunc.fastpayment.service;

import java.util.List;
import ru.rkunc.fastpayment.domain.Payment;
import ru.rkunc.fastpayment.domain.User;

/**
 *
 * @author rkunc
 */
public interface PaymentService {
    
    public boolean compareAmountAndUserBalance(Payment payment);
    
    public boolean compareAmountAndServiceLimits(Payment payment);
    
    public void addPayment(Payment payment);
    
    public List<Payment> listPayments(User user);
}
