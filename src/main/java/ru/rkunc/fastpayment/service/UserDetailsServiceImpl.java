/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.rkunc.fastpayment.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.rkunc.fastpayment.dao.RoleDAO;
import ru.rkunc.fastpayment.dao.UserDAO;
import ru.rkunc.fastpayment.domain.Role;
import ru.rkunc.fastpayment.domain.User;

/**
 *
 * @author rkunc
 */
@Service("authService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private RoleDAO roleDAO;

    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException, DataAccessException {

        User user = userDAO.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("user not found");
        }

        return buildUserFromUserEntity(user);
    }

    private UserDetails buildUserFromUserEntity(User user) {

        String username = user.getLogin();
        String password = user.getPassword();
        
        List<GrantedAuthority> authorities = new ArrayList<>();
        List<Role> roles = roleDAO.findByUsername(username);
        for (Role role : roles) {
            authorities.add(new SimpleGrantedAuthority(role.getAuthority()));
        }

        org.springframework.security.core.userdetails.User userDetails = new org.springframework.security.core.userdetails.User(username, password, authorities);
        return userDetails;
    }
}
