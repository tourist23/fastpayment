/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.rkunc.fastpayment.service;

/**
 *
 * @author rkunc
 */
public enum CustomRole {

    USER("USER");
    
    private final String name;

    private CustomRole(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    
}
