/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.rkunc.fastpayment.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.rkunc.fastpayment.dao.ServiceProfileDAO;
import ru.rkunc.fastpayment.domain.ServiceProfile;

/**
 *
 * @author rkunc
 */
@Service
public class ServiceProfileServiceImpl implements ServiceProfileService{

    @Autowired
    private ServiceProfileDAO serviceProfileDAO;

    @Override
    public ServiceProfile getProfile(String name) {
        if(name == null){
            throw new IllegalArgumentException("A name of a profile service is null");
        }
        return serviceProfileDAO.findByName(name);
    }
    
    @Override
    public List<ServiceProfile> getProfiles() {
        return serviceProfileDAO.findAll();
    }

    
}
