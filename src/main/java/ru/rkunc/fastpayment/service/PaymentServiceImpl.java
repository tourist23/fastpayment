/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.rkunc.fastpayment.service;

import java.math.BigDecimal;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.rkunc.fastpayment.dao.PaymentDAO;
import ru.rkunc.fastpayment.dao.ServiceProfileDAO;
import ru.rkunc.fastpayment.dao.UserDAO;
import ru.rkunc.fastpayment.domain.Payment;
import ru.rkunc.fastpayment.domain.ServiceProfile;
import ru.rkunc.fastpayment.domain.User;

/**
 *
 * @author rkunc
 */
@Service
public class PaymentServiceImpl implements PaymentService{

    private static final Logger logger = LogManager.getLogger(PaymentServiceImpl.class);
    
    @Autowired
    private PaymentDAO paymentDAO;
    
    @Autowired 
    private UserDAO userDAO;
    
    @Autowired
    private ServiceProfileDAO serviceProfileDAO;
    
    @Override
    public List<Payment> listPayments(User user) {
        if(user == null){
            throw new IllegalArgumentException("User is null");
        }
        logger.info("Получение списка платежей для пользователя {}", user.getLogin());
        return paymentDAO.findByUsername(user.getLogin());
    }

    @Override
    @Transactional
    public void addPayment(Payment payment) {
        logger.info("Создание нового платежа");
        if(payment == null){
            throw new IllegalArgumentException("Payment is null");
        }
        logger.info("Параметры платежа: {}", payment);
        
        User user = userDAO.findByUsername(payment.getCreatedBy());
        
        logger.info("Вычисление остатка на балансе пользователя {}", user.getBalance());
        BigDecimal newAmount = user.getBalance().subtract(payment.getAmount());
        logger.info("Новый баланс: {}", newAmount);
        
        logger.info("Создание платежа в базе");
        paymentDAO.insert(payment);

        logger.info("Обновление баланса пользователя в базе");
        user.setBalance(newAmount);
        userDAO.update(user);
        
        logger.info("Создание платежа успешно завершено");
    }

    @Override
    public boolean compareAmountAndServiceLimits(Payment payment) {
        logger.info("Сравнение суммы платежа с мин. и мак. значением из профиля услуги");
        if(payment == null){
            throw new IllegalArgumentException("Payment is null");
        }
        ServiceProfile profile = serviceProfileDAO.findByName(payment.getServiceName());
        
        BigDecimal amount = payment.getAmount();
        BigDecimal minAmount = profile.getMinAmount();
        BigDecimal maxAmount = profile.getMaxAmount();
        logger.info("Сумма платежа: {}; Мин. сумма: {}; Макс. сумма: {}", amount, minAmount, maxAmount);
        
        return compareAmountAndMinAmount(amount, minAmount) && compareAmountAndMaxAmount(amount, maxAmount);
    }
    
    private boolean compareAmountAndMaxAmount(BigDecimal amount, BigDecimal maxAmount){
        logger.info("Сравнение суммы платежа с мак. значением из профиля услуги");
        if(amount == null){
            throw new IllegalArgumentException("Amount is null");
        }
        if(maxAmount == null){
            throw  new IllegalArgumentException("MaxAmount is null");
        }
        int result = amount.compareTo(maxAmount);
        return result == 0 || result == -1;
    }
    
    private boolean compareAmountAndMinAmount(BigDecimal amount, BigDecimal minAmount){
        logger.info("Сравнение суммы платежа с мин. значением из профиля услуги");
        if(amount == null){
            throw new IllegalArgumentException("Amount is null");
        }
        if(minAmount == null){
            throw  new IllegalArgumentException("MinAmount is null");
        }
        int result = amount.compareTo(minAmount);
        return result == 0 || result == 1;
    }

    @Override
    public boolean compareAmountAndUserBalance(Payment payment) {
        logger.info("Сравнение суммы платежа с балансом пользователя");
        if(payment == null){
            throw new IllegalArgumentException("Payment is null");
        }
        User user = userDAO.findByUsername(payment.getCreatedBy());
        
        BigDecimal amount = payment.getAmount();
        BigDecimal balance = user.getBalance();
        logger.info("Сумма платежа: {}; Баланс: {}", amount, balance);
        
        return compareAmountAndBalance(amount, balance);
    }

    private boolean compareAmountAndBalance(BigDecimal amount, BigDecimal balance){
        if(amount == null){
            throw new IllegalArgumentException("Amount is null");
        }
        if(balance == null){
            throw  new IllegalArgumentException("Balance is null");
        }
        int result = amount.compareTo(balance);
        return result == 0 || result == -1;
    }
    
}
