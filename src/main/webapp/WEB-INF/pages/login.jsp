<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf8">
	<title><spring:message code="label.title" /></title>
        <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
</head>
<body>
    <div class="container-fluid">
        <fieldset>
        <legend><spring:message code="label.legend.login"/></legend>
        <form class="form-horizontal" name="loginForm" th:action="@{/login}" method="post"> 
            <c:if test="${param.error ne null}">
                <div class="form-group">
                    <label class="col-sm-2 control-label bg-warning"><spring:message code="label.loginerror"/> : ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message} </label>
                </div>
            </c:if>
            <div class="form-group">
                <label class="col-sm-1 control-label"><spring:message code="label.login"/></label>
                <div class="col-sm-3">
                    <input class="form-control" type="text" name="username" id="username" placeholder="Login">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-1 control-label"><spring:message code="label.password"/></label>
                <div class="col-sm-3">
                    <input class="form-control" type="password" name="password" id="password" placeholder="Password">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-1 col-sm-10">
                    <button type="submit" class="btn btn-success"><spring:message code="label.button.login"/></button>
                    <button type="button" class="btn btn-default" onclick="javascript:window.location='registration';"><spring:message code="label.button.registration"/></button>
                </div>
            </div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </form>
        </fieldset>
    </div>
</body>
</html>