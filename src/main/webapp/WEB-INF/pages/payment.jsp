<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="_csrf" content="${_csrf.token}"/>
        <meta name="_csrf_header" content="${_csrf.headerName}"/>
        
        <title><spring:message code="label.title" /></title>
        
        <script src="<c:url value='/resources/js/jquery.min.js'/>"></script>
        <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
        
        
        <script type="text/javascript">
            $(document).ready(function() {
                $(function (){
                    $('#serviceName').on('change', function() {
                        var name = $('#serviceName').val();

                        var token = $("meta[name='_csrf']").attr("content");
                        var header = $("meta[name='_csrf_header']").attr("content");
                        
                        $.ajax({
                          url: '${pageContext.request.contextPath}/payment/getService',
                          data: 'serviceName='+name,

                          beforeSend: function(xhr) {
                              xhr.setRequestHeader(header, token);
                          },
                          success: function(response){  
                              $('#minAmount').val(response.minAmount);
                              $('#maxAmount').val(response.maxAmount);
                          },  
                          error: function(e){ 
                            alert('Error: ' + e);  
                          } 
                          });
                    });
                });
            });
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="text-right"><button type="button" class="btn btn-link" onclick="javascript:window.location='logout';"/><spring:message code="label.logout"/></button></div>
            <legend>
                <ul class="list-inline">
                    <li><button type="button" class="btn btn-default btn-lg btn-block" onclick="javascript:window.location='index';"><spring:message code="label.legend.cabinet"/></button></li>
                    <li><button type="button" class="btn btn-primary btn-lg btn-block" onclick="javascript:window.location='payment';"><spring:message code="label.payment.create"/></button></li>
                </ul>
            </legend>
            <form:form commandName="newPayment" cssClass="form-horizontal" action="payment">
                <c:if test="${successMsg != null}" >
                    <div class="form-group has-success"><label class="col-sm-2 control-label success"><spring:message code="label.payment.successfully"/></div>
                </c:if>
                
                <div class="form-group">
                    <label class="col-sm-1 control-label"><spring:message code="label.service"/></label>
                    <div class="col-sm-3">
                        <form:select path="serviceName" id="serviceName" class="form-control">
                            <form:options items="${services}" itemValue="name"   itemLabel="name"></form:options>
                        </form:select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label"><spring:message code="label.account"/></label>
                    <div class="col-sm-3">
                        <form:input class="form-control" path="account" maxlength="40" pattern="[0-9]{1,40}" required="true" placeholder="Пример: 1234567890"/>
                    </div>
                    <div class="has-error">
                        <form:errors path="account" class="control-label"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label"><spring:message code="label.min_amount"/></label>
                    <div class="col-sm-1">
                        <input class="form-control" id="minAmount" readonly="true" value="${services[0].minAmount}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label"><spring:message code="label.max_amount"/></label>
                    <div class="col-sm-1">
                        <input class="form-control" id="maxAmount" readonly="true" value="${services[0].maxAmount}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label"><spring:message code="label.amount"/></label>
                    <div class="col-sm-1">
                        <form:input class="form-control" path="amount" pattern="^\d+(\.\d{1,2})?" required="true" placeholder="Пример: 10.00"/>
                    </div>
                    <div class="has-error">
                        <form:errors path="amount" class="control-label"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-1 col-sm-10">
                        <button type="submit" class="btn btn-success"><spring:message code="label.pay"/></button>
                    </div>
                </div>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </form:form>
        </div>
    </body>
</html>
