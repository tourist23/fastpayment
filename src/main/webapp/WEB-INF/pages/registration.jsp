<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="_csrf" content="${_csrf.token}"/>
        <meta name="_csrf_header" content="${_csrf.headerName}"/>
        
        <title><spring:message code="label.title" /></title>
        
        <script src="<c:url value='/resources/js/jquery.min.js'/>"></script>
        <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
        
<!--        <script type="text/javascript">
        $(document).ready(function() {
 
            $('#submitForm').submit(function(e) {  
            var frm = $('#submitForm');
            var account = $('#login').val();
            var json = { "login" : account};
            
            var token = $("meta[name='_csrf']").attr("content");
            var header = $("meta[name='_csrf_header']").attr("content");
            
            $.ajax({
                type: frm.attr('method'),
                url: frm.attr('action'),
              
//              url: "${pageContext.request.contextPath}/registration/create",
              data: JSON.stringify(json),
//              type: "POST",  
              contentType: 'application/json',

              beforeSend: function(xhr) {
                  xhr.setRequestHeader("Accept", "application/json");
                  xhr.setRequestHeader("Content-Type", "application/json");
                  xhr.setRequestHeader(header, token);
              },
              success: function(response){  
                  $('#password').val(response.password);
//                  $('#error').hide('slow');
              },  
              error: function(e){ 
                alert('Error: ' + e);  
              }  
            });
		});
	});
        </script>-->
    </head>
    <body>
        <div class="container-fluid">
            <legend><spring:message code="label.legend.registration"/></legend>
            <form:form id="submitForm" commandName="user" cssClass="form-horizontal" action="registration" >
                <c:if test="${successMsg != null}" >
                    <div class="form-group has-success"><label class="col-sm-2 control-label success"><spring:message code="label.registration.successfully"/></div>
                </c:if>
                <div class="form-group">
                    <label class="col-sm-1 control-label"><spring:message code="label.login"/></label>
                    <div class="col-sm-3">
                        <form:input class="form-control" required="true" path="login"  placeholder="Пример: 9xxxxxxxxx, user@mail.com"/>
                    </div>
                    <div class="has-error">
                        <form:errors path="login" class="control-label has-error"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label"><spring:message code="label.password"/></label>
                    <div class="col-sm-3">
                        <form:input class="form-control" path="password" value="${createUser.password}" readonly="true" placeholder=""/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-1 col-sm-10">
                        <button type="submit" class="btn btn-success" ><spring:message code="label.button.registration"/></button>
                        <button type="button" class="btn btn-default"  onclick="javascript:window.location='login';"><spring:message code="label.goback"/></button>
                    </div>
                </div>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </form:form>
        </div>
    </body>
</html>
