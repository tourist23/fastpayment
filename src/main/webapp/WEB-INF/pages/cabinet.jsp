<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf8">
	<title><spring:message code="label.title" /></title>
        <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
</head>
    <body>
        <div class="container-fluid">
            <div class="text-right"><button type="button" class="btn btn-link" onclick="javascript:window.location='logout';"/><spring:message code="label.logout"/></button></div>
            <legend>
                <ul class="list-inline">
                    <li><button type="button" class="btn btn-primary btn-lg btn-block" onclick="javascript:window.location='index';"><spring:message code="label.legend.cabinet"/></button></li>
                    <li><button type="button" class="btn btn-default btn-lg btn-block" onclick="javascript:window.location='payment';"><spring:message code="label.payment.create"/></button></li>
                </ul>
            </legend>
            <div class="row">
                <div class="col-md-4"><label><spring:message code="label.login"/>: ${user.login}</label></div>
            </div>
            <div class="row">
                <div class="col-md-4"><label><spring:message code="label.balance"/>: ${user.balance}</label></div>
            </div>
        </div>
        <div>
            <table class="table table-striped">
                <caption class="text-center"><spring:message code="label.table.listpayments"/></caption>
                <tr>
                 <th>#</th>
                 <th>Номер счёта</th>
                 <th>Сумма</th>
                 <th>Услуга</th>
                 <th>Дата создания</th>
                </tr>
                <c:forEach items="${payments}" var="payment" varStatus="loop">
                    <tr>
                        <td><c:out value="${loop.index+1}"/></td>
                        <td><c:out value="${payment.account}"/></td>
                        <td><c:out value="${payment.amount}"/></td>
                        <td><c:out value="${payment.serviceName}"/></td>
                        <td><c:out value="${payment.created}"/></td>
                    </tr>
                </c:forEach>
            </table>
        </div>
        
    </body>
</html>
